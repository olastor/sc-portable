stages:
  - prepare
  - build
  - bundle

variables:
  # pin version to use when cloning sc
  # TODO: this is latest master, change to latest prod which was not working
  SC_GIT_REF: daa0d97131e4f637b7d24716803ee0709fdbabb6
  COSMO_GIT_REF: a85406da4c8a25151fdd7aa988f88394f4d9d004

.build-with-sc:
  image: docker/compose
  services:
    - docker:20-dind
  allow_failure:
    exit_codes:
      - 0
      - 44
  before_script:
    - |
      if [ ! -z "$SKIP_IF_CACHED" ] && [ -e "$SKIP_IF_CACHED" ]; then
        exit 44
      fi
    - apk add --no-cache curl wget git make bash openssl
    - mv suttacentral/volumes .
    - rm -rf suttacentral
    - git clone https://github.com/suttacentral/suttacentral
    - mv volumes suttacentral
    - cd suttacentral
    - git checkout $SC_GIT_REF
    # those are not needed but not starting them makes nginx fail
    # - sed -i 's/sc-swagger//g' Makefile
    # - sed -i 's/sc-frontend//g' Makefile
    # - sed -i 's/sc-chrome-headless//g' Makefile
    - sed -i 's/sc-elasticsearch//g' Makefile
    # switch to local volumes for data we want to cache
    - |
      if [ ! -d volumes ]; then
        mkdir -p volumes
        export NO_CACHE="1"
      fi
    - sed -i 's,db-data-volume:/,\./volumes/db-data:/,g' docker-compose.yml
    - sed -i 's,sc-data-volume:/,\./volumes/sc-data:/,g' docker-compose.yml
    - sed -i '/db-data-volume:/d' docker-compose.yml
    - sed -i '/sc-data-volume:/d' docker-compose.yml
    - make create-network
    - make generate-cert
    - make run-dev-no-logs
    - bash wait_for_flask.sh
    - |
      if [ ! -z "$NO_CACHE" ]; then
        make load-data
        make hyphenate
      fi

build data pre:
  extends: .build-with-sc
  stage: prepare
  variables:
    SKIP_IF_CACHED: "suttacentral/volumes"
  script:
    - echo "Done!"
  cache:
    key: "${SC_GIT_REF}-data-volumes" # TODO: better key
    paths:
      - "./suttacentral/volumes"

build client:
  extends: .build-with-sc
  stage: build
  needs: ["build data pre"]
  variables:
    SKIP_IF_CACHED: "client/"
  script:
    - export FRONTEND_IMAGE=$(docker ps | grep sc-frontend | cut -d' ' -f1)
    - docker exec $FRONTEND_IMAGE bash -c "sed -i '/BundleAnalyzerPlugin/d' webpack.common.js && node_modules/.bin/webpack --no-watch --config webpack.prod.js"
    - docker cp $FRONTEND_IMAGE:/opt/sc/frontend/build .
    - mv ./build ../client
  cache:
    - key: "${SC_GIT_REF}-data-volumes" # TODO: better key
      paths:
        - "./suttacentral/volumes"
      policy: pull
    - key: "${SC_GIT_REF}-frontend" # TODO: better key
      paths:
        - "./client"
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
      - "client/"

build data:
  extends: .build-with-sc
  stage: build
  timeout: 2h
  needs: ["build data pre"]
  variables:
    SKIP_IF_CACHED: "api/"
  script:
    - mv ../import.py ./server/server/
    - make run-preview-env-no-search
    - export FLASK_IMAGE=$(docker ps | grep sc-flask | cut -d' ' -f1)
    - docker exec $FLASK_IMAGE python3 server/import.py
    - mv server/out ../api
  cache:
    - key: "${SC_GIT_REF}-data-volumes" # TODO: better key
      paths:
        - "./suttacentral/volumes"
      policy: pull
    - key: "${SC_GIT_REF}-api" # TODO: better key
      paths:
        - "./api"
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
      - "api/"

build redbean:
  stage: build
  image: ubuntu:20.04
  when: manual
  before_script:
    - apt-get update
    - apt-get install -y git make
  script:
    - |
      if [ ! -d "binaries/" ]; then
        git clone https://github.com/jart/cosmopolitan.git
        cd cosmopolitan
        git checkout $COSMO_GIT_REF
        make -j8 o//tool/net/redbean.com >> build.log 2>&1
        mv build.log ..
        mv o/tool/net ../binaries
      fi
  cache:
    key: "$COSMO_GIT_REF"
    paths:
      - binaries/
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
      - binaries/
      - "*.log"

.bundle:
  stage: bundle
  image: alpine:3.15
  before_script:
    - apk add --no-cache bash curl zip grep jq
    - mv binaries/net/redbean.com .
    - '[ ! -f redbean.com ] && exit 1'
    - |
      if [ ! -d api ]; then
        curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/35708538/jobs?per_page=100" > api.json
        export JOB_ID=$(cat api.json | jq '[.[] | select(.name=="build data") | select(.status=="success")][0].id')
        curl -L "https://gitlab.com/olastor/sc-portable/-/jobs/$JOB_ID/artifacts/download" > artifacts.zip
        unzip -q artifacts.zip
      fi
  cache:
    key: "$COSMO_GIT_REF"
    paths:
      - binaries/
    policy: pull
  artifacts:
    untracked: false
    # expire_in: 30 days
    paths:
      - "*.com"
      - "o/"

create bundle:
  extends: .bundle
  when: manual
  script:
    - ./package.sh "en" "redbean.com"
